package com.example.mycounterapplication

import android.app.Service
import android.content.Intent
import android.os.Binder
import android.os.IBinder
import android.widget.Toast

class MyService : Service() {
    private var counter = 0

    inner class MyServiceBinder : Binder() {
        fun getService() = this@MyService
    }

    fun increment() {
        counter++
    }

    fun decrement() {
        counter--
    }
    fun presentValue() {
        Toast.makeText(this, "$counter", Toast.LENGTH_LONG).show()
    }


    override fun onBind(intent: Intent): IBinder {
        return MyServiceBinder()
    }
}
