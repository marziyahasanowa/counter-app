package com.example.mycounterapplication

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.Bundle
import android.os.IBinder
import android.view.View
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.mycounterapplication.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        initUI()

    }
    private lateinit var binding: ActivityMainBinding
    private var myService: MyService? = null
    private var isBound = false

    private val serviceConnection = object : ServiceConnection {
        override fun onServiceConnected(p0: ComponentName?, service: IBinder?) {
            val binder = service as MyService.MyServiceBinder
            myService = binder.getService()
            isBound = true
        }

        override fun onServiceDisconnected(name: ComponentName?) {
            isBound = false
            myService = null
        }
    }

    private fun initUI() {
        val increment_btn = findViewById<Button>(R.id.increment_btn)
        val decrement_btn = findViewById<Button>(R.id.decrement_btn)
        val present_value_btn = findViewById<Button>(R.id.present_value_btn)
        binding.apply {
            increment_btn.setOnClickListener{
                myService?.increment()
            }
            decrement_btn.setOnClickListener {
                myService?.decrement()
            }
            present_value_btn.setOnClickListener {
                myService?.presentValue()
            }
        }
    }

    override fun onStart() {
        super.onStart()
        val intent = Intent(this, MyService::class.java)
        startService(intent)
        bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE)
    }

    override fun onStop() {
        super.onStop()
        if (isBound) unbindService(serviceConnection)

    }
}





